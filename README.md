# Gyro Noise Characterisation

MATLAB script for characterizing a gyroscope. The gyroscope shall operate
in maximum sampling frequency for a long period of time ( > 30 minutes)

