clear;clc;close all
set(0,'DefaultFigureVisible','off')

%Samples should be contained in a file called samples.log
% Data should be contained in first column of the array. The second column
% shall contain the time of sampling at that particular moment or containg 
% nothing if frequency sampling is defined.
data = load('samples.log');
fs = 0; %sampling frequency, should be defined.

%--------------------------------------------------------------------------
% In case fs is not known, but sampling timing is saved to an extra column
% in samples.log, uncommment the following segment. Time should be measured
% in seconds, float datatype
%                                 
% time_column = 2;
% len = length(data);
% fs = len/(data(end,time_column)-data(1,time_column))
%--------------------------------------------------------------------------

[ARW, RRW, BI] = characterization(data(:,1), fs)


