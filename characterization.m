function [ARW, RRW, BI] = characterization(data, sampling_frequency)
%CHARACTERIZATION uses gyroscope samples to characterize its noise. Plots
%the allan deviation with the angle random walk(ARW), the rate random walk (RRW) 
%and the bias. 
%Returns the ARW, the RRW and the bias instability as values.
%   data (column array) : N samples of the gyroscope
%   sampling_frequency (float) : The frequency the samples were taken,
%   measured in Hz

    fs = sampling_frequency;
    t0 = 1/fs; %fs = 219-228Hz
    theta = cumsum(data(:,1))*t0;

    maxNumM = 100;
    L = size(theta, 1);
    maxM = 2.^floor(log2(L/2));
    m = logspace(log10(1), log10(maxM), maxNumM).';
    m = ceil(m); % m must be an integer.
    m = unique(m); % Remove duplicates.

    tau = m*t0;

    avar = zeros(numel(m), 1);
    for i = 1:numel(m)
        mi = m(i);
        avar(i,:) = sum( ...
            (theta(1+2*mi:L) - 2*theta(1+mi:L-mi) + theta(1:L-2*mi)).^2, 1);
    end
    avar = avar ./ (2*tau.^2 .* (L - 2*m)); %allan variance

    adev = sqrt(avar); % allan deviation

    figure
    loglog(tau, adev)
    title('Allan Deviation')
    xlabel('\tau');
    ylabel('\sigma(\tau)')
    grid on
    axis equal

    % Find the index where the slope of the log-scaled Allan deviation is equal
    % to the slope specified.
    slope = -0.5;
    logtau = log10(tau);
    logadev = log10(adev);
    dlogadev = diff(logadev) ./ diff(logtau);
    [~, i] = min(abs(dlogadev - slope));

    % Find the y-intercept of the line.
    b = logadev(i) - slope*logtau(i);

    % Determine the angle random walk coefficient from the line.
    logN = slope*log(1) + b;
    ARW = 10^logN %
    N = ARW; %ARW
    % Plot the results.
    tauN = 1;
    lineN = N ./ sqrt(tau);
    figure
    loglog(tau, adev, tau, lineN, '--', tauN, N, 'o')
    title('Allan Deviation with Angle Random Walk')
    xlabel('\tau')
    ylabel('\sigma(\tau)')
    legend('\sigma', '\sigma_N')
    text(tauN, N, 'N')
    grid on
    axis equal

    % Find the index where the slope of the log-scaled Allan deviation is equal
    % to the slope specified.
    slope = 0.5;
    logtau = log10(tau);
    logadev = log10(adev);
    dlogadev = diff(logadev) ./ diff(logtau);
    [~, i] = min(abs(dlogadev - slope));

    % Find the y-intercept of the line.
    b = logadev(i) - slope*logtau(i);

    % Determine the rate random walk coefficient from the line.
    logK = slope*log10(3) + b;
    RRW = 10^logK %Rate Random Walk
    K = RRW;
    % Plot the results.
    tauK = 3;
    lineK = K .* sqrt(tau/3);
    figure
    loglog(tau, adev, tau, lineK, '--', tauK, K, 'o')
    title('Allan Deviation with Rate Random Walk')
    xlabel('\tau')
    ylabel('\sigma(\tau)')
    legend('\sigma', '\sigma_K')
    text(tauK, K, 'K')
    grid on
    axis equal

    % Find the index where the slope of the log-scaled Allan deviation is equal
    % to the slope specified.
    slope = 0;
    logtau = log10(tau);
    logadev = log10(adev);
    dlogadev = diff(logadev) ./ diff(logtau);
    [~, i] = min(abs(dlogadev - slope));

    % Find the y-intercept of the line.
    b = logadev(i) - slope*logtau(i);

    % Determine the bias instability coefficient from the line.
    scfB = sqrt(2*log(2)/pi);
    logB = b - log10(scfB);
    BIAS = 10^logB;
    B = BIAS;
    BI = BIAS^2;
    % Plot the results.
    tauB = tau(i);
    lineB = B * scfB * ones(size(tau));
    figure
    loglog(tau, adev, tau, lineB, '--', tauB, scfB*B, 'o')
    title('Allan Deviation with Bias Instability')
    xlabel('\tau')
    ylabel('\sigma(\tau)')
    legend('\sigma', '\sigma_B')
    text(tauB, scfB*B, '0.664B')
    grid on
    axis equal

    tauParams = [tauN, tauK, tauB];
    params = [N, K, scfB*B];
    figure
    loglog(tau, adev, tau, [lineN, lineK, lineB], '--', ...
        tauParams, params, 'o')
    title('Allan Deviation with Noise Parameters')
    xlabel('\tau')
    ylabel('\sigma(\tau)')
    legend('$\sigma (rad/s)$', '$\sigma_N ((rad/s)/\sqrt{Hz})$', ...
        '$\sigma_K ((rad/s)\sqrt{Hz})$', '$\sigma_B (rad/s)$', 'Interpreter', 'latex')
    text(tauParams, params, {'N', 'K', '0.664B'})
    grid on
    axis equal
end